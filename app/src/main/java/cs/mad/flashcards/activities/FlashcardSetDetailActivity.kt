package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.getHardcodedFlashcards
import java.io.Serializable


class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.flashcardList.adapter = FlashcardAdapter(getHardcodedFlashcards())

        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(Flashcard("test", "test"))
                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        val studyButton = findViewById<MaterialButton>(R.id.study_set_button)
        studyButton.setOnClickListener {
            val adapter = binding.flashcardList.adapter as FlashcardAdapter
            val intent = Intent(this, StudySetActivity::class.java)
            intent.putExtra("dataSet", adapter.dataSet as Serializable)
            startActivity(intent)
        }

    }

    fun showEditableAlert(position: Int) {
        val adapter = binding.flashcardList.adapter as FlashcardAdapter

        val titleView = layoutInflater.inflate(R.layout.custom_title, null)
        val bodyView = layoutInflater.inflate(R.layout.custom_body, null)
        AlertDialog.Builder(this)
            .setCustomTitle(titleView)
            .setView(bodyView)
            .setPositiveButton("Done") { _: DialogInterface, _: Int ->
                val newQuestion = titleView.findViewById<EditText>(R.id.custom_title_field).text
                val newAnswer = bodyView.findViewById<EditText>(R.id.custom_body_field).text
                adapter.dataSet[position].question = newQuestion.toString()
                adapter.dataSet[position].answer = newAnswer.toString()
                adapter.notifyItemChanged(position)
            }
            .setNegativeButton("Delete") { _: DialogInterface, _: Int ->

                adapter.dataSet.removeAt(position)
                adapter.notifyItemRemoved(position)
                adapter.notifyItemRangeChanged(0, adapter.dataSet.size)
            }
            .create()
            .show()
        titleView.findViewById<EditText>(R.id.custom_title_field).setText(adapter.dataSet[position].question)
        bodyView.findViewById<EditText>(R.id.custom_body_field).setText(adapter.dataSet[position].answer)
    }
}