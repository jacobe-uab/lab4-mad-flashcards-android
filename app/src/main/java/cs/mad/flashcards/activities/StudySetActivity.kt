package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class StudySetActivity : AppCompatActivity() {

    private var missed = 0;
    private var correct = 0;
    private var completed = 0
    private val missedCards: MutableList<Flashcard> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_set)

        val cardView = findViewById<CardView>(R.id.card_view)
        val questionText = findViewById<TextView>(R.id.question_text)
        val answerText = findViewById<TextView>(R.id.answer_text)
        val exitButton = findViewById<Button>(R.id.exit_button)
        val skipButton = findViewById<Button>(R.id.skip_button)
        val missButton = findViewById<Button>(R.id.miss_button)
        val missCountText = findViewById<TextView>(R.id.miss_count)
        val correctButton = findViewById<Button>(R.id.correct_button)
        val correctCountText = findViewById<TextView>(R.id.correct_count)
        val completedCountText = findViewById<TextView>(R.id.completed_count)

        val dataSet = intent.getSerializableExtra("dataSet") as MutableList<Flashcard>
        val startSize = dataSet.count()

        questionText.text = dataSet[0].question
        answerText.text = dataSet[0].answer
        completedCountText.text = "Completed: $completed/$startSize"

        cardView.setOnClickListener {
            if (answerText.visibility == View.VISIBLE) {
                answerText.visibility = View.INVISIBLE
            } else {
                answerText.visibility = View.VISIBLE
            }
        }

        exitButton.setOnClickListener {
            finish()
        }

        skipButton.setOnClickListener {
            if (dataSet.count() == 0) {
                showDoneAlert()
            } else {
                dataSet.add(dataSet[0])
                dataSet.removeFirst()

                questionText.text = dataSet[0].question
                answerText.text = dataSet[0].answer

                answerText.visibility = View.INVISIBLE
            }
        }

        missButton.setOnClickListener {
            if (dataSet.count() == 0){
                showDoneAlert()
            } else {
                if (dataSet[0] !in missedCards) { missedCards.add(dataSet[0]) }
                missed = missedCards.count()

                dataSet.add(dataSet.size, dataSet[0])
                dataSet.removeFirst()

                questionText.text = dataSet[0].question
                answerText.text = dataSet[0].answer
                missCountText.text = "$missed"

                answerText.visibility = View.INVISIBLE
            }
        }

        correctButton.setOnClickListener {
            if (dataSet.count() == 0) {
                showDoneAlert()
            } else if (dataSet.count() == 1) {
                if (dataSet[0] !in missedCards) { correct++ }
                completed++

                dataSet.removeFirst()

                questionText.text = ""
                answerText.text = ""
                correctCountText.text = "$correct"
                completedCountText.text = "Completed: $completed/$startSize"

                showDoneAlert()
            } else {
                if (dataSet[0] !in missedCards) { correct++ }
                completed++

                dataSet.removeFirst()
                questionText.text = dataSet[0].question
                answerText.text = dataSet[0].answer
                correctCountText.text = "$correct"
                completedCountText.text = "Completed: $completed/$startSize"

                answerText.visibility = View.INVISIBLE
            }
        }
    }

    fun showDoneAlert() {
        AlertDialog.Builder(this)
            .setTitle("You have studied all of your flashcards!")
            .setMessage("Correct: $correct\nIncorrect: $missed")
            .setPositiveButton("DONE") { _: DialogInterface, _: Int ->
                finish()
            }
            .create()
            .show()
    }
}